//
//  NullReplacement.swift
//  NewsDemo
//
//  Created by kushal on 04/07/19.
//  Copyright © 2019 ajeet. All rights reserved.
//

import Foundation

extension NSDictionary {
    @objc func dictionaryByReplacingNullsWithBlanks() -> NSDictionary {
        let replaced:NSMutableDictionary = self.mutableCopy() as! NSMutableDictionary
        
        let blank = ""
        
        for strKey in self.keyEnumerator()
        {
            let object:AnyObject = self.object(forKey: strKey) as AnyObject
            
            if object is NSNull
            {
                replaced.setObject(blank, forKey: strKey as! NSCopying)
            }
            else if object is NSDictionary
            {
                replaced.setObject(object.dictionaryByReplacingNullsWithBlanks(), forKey: strKey as! NSCopying)
            }
            else if object is NSArray
            {
                replaced.setObject((object as! NSArray).arrayByReplacingNullsWithBlanks(), forKey: strKey as! NSCopying)
            }
        }
        
        return replaced.copy() as! NSDictionary
    }
}

extension NSArray {
    func arrayByReplacingNullsWithBlanks() -> NSArray {
        let replaced: NSMutableArray = self.mutableCopy() as! NSMutableArray
        let blank = ""
        
        for idx in 0..<replaced.count {
            let object = replaced.object(at: idx)
            if object is NSNull {
                replaced.replaceObject(at: idx, with: blank)
            } else if object is NSDictionary {
                replaced.replaceObject(at: idx, with: (object as! NSDictionary).dictionaryByReplacingNullsWithBlanks())
            } else if object is NSArray {
                replaced.replaceObject(at: idx, with: (object as! NSArray).arrayByReplacingNullsWithBlanks())
            }
        }
        
        return replaced.copy() as! NSArray
    }
}
