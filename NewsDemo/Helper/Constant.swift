//
//  Constant.swift
//  NewsDemo
//
//  Created by kushal on 03/07/19.
//  Copyright © 2019 ajeet. All rights reserved.
//

import Foundation
import UIKit

struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    
}
//MARK:- Internet Validtaion Message
let kNoInternetConnection = "No Internet Connection"
let kInternetError = "Please check your internet connetion"


