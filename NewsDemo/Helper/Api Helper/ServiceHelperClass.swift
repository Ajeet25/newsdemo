//
//  ServiceHelperClass.swift
//  Zango
//
//  Created by Aarti on 13/04/18.
//  Copyright © 2018 Nine Hertz India. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

func getTokenApiResults(Details: NSDictionary,srtMethod : String, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    
    
    let url = Base_URL + srtMethod
    print("getAllApiResults: ",url)
    print("getAllApiResults: ",Details)
    
    Alamofire.request(url, method: .post, parameters: Details as? Parameters, encoding: JSONEncoding.default, headers: nil).responseJSON{ response in
        DispatchQueue.main.async {
            completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
            print(response.result.value)
            print(response.result.error)
        }
        
        #if DEBUG
            //print("signUpResponse Debug Data: \(String(describing: response.result.value))")
        #endif
    }
}

// Check User

func getAllApiResults(Details: NSDictionary,srtMethod : String, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    let url = Base_URL + srtMethod
    print("ApiURL ==========>> ",url)
    print("ApiParameters: ==========>>",Details)
    
    
    Alamofire.request(url, method: .post, parameters: Details as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON{ response in
        print("API_Response ========>> ", response)
        DispatchQueue.main.async {
            completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
          
        }
        
        #if DEBUG
            //print("signUpResponse Debug Data: \(String(describing: response.result.value))")
        #endif
    }
}

func getallApiResultwithPostTokenMethod(Details: NSDictionary,strMethodname : String, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    var strToken = ""
    if UserDefaults.standard.object(forKey: kServerTokenKey) != nil
    {
        strToken = UserDefaults.standard.object(forKey: kServerTokenKey)! as! String
    }
    print("Token ==========>> Authorization",  "Bearer " + strToken)
    
    let headers: HTTPHeaders = [
        "Content-Type":"application/json",
        "Accept": "application/json",
        "Authorization":  "Bearer " + strToken
        
    ]
    print(" Header =====>>> \(headers)")
    let url = Base_URL + strMethodname
      print("ApiURL ==========>>",url)
      print("ApiParameters: ==========>>",Details)
    
    Alamofire.request(url, method: .post, parameters: Details as? Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ response in
        DispatchQueue.main.async {
            completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
             print("API_Response ========>> ", response)
             print("Error ========>> ",response.result.error)
            
        }
        
        #if DEBUG
        //print("signUpResponse Debug Data: \(String(describing: response.result.value))")
        #endif
    }
}

func getallApiResultwithimagePostMethod(strMethodname : String, imgData : Data, strImgKey : String, Details: NSDictionary, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    
   // let strToken = UserDefaults.standard.object(forKey: kServerTokenKey)! as! String
    
    let url = Base_URL + strMethodname
     print("ApiURL ==========>>",url)
     print("ApiParameters: ==========>>",Details)
    
    
    var strToken = ""
    if UserDefaults.standard.object(forKey: kServerTokenKey) != nil
    {
        strToken = UserDefaults.standard.object(forKey: kServerTokenKey)! as! String
    }
    print("Token ==========>> Authorization",  "Bearer " + strToken)

    let headers: HTTPHeaders = [
         "Content-Type":"application/json",
         "Accept": "application/json",
        "Authorization":  "Bearer " + strToken
    ]
    
    Alamofire.upload(multipartFormData:
        { multipartFormData in
            
            if imgData.count > 0
            {
                multipartFormData.append((imgData), withName: strImgKey, fileName: "file.png", mimeType: "image/jpeg")
            }
            for (key, val) in Details
            {
                multipartFormData.append((val as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
            
    },to: url,method:.post,headers:headers, encodingCompletion:
        { encodingResult in
            switch encodingResult
            {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    DispatchQueue.main.async {
                        completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
                        print("API_Response ========>> ", response)
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
            }
    }
        
    )
}

func getallApiResultwithGetMethod(strMethodname : String,Details: NSDictionary, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    
    var strToken = ""
    if UserDefaults.standard.object(forKey: kServerTokenKey) != nil
    {
        strToken = UserDefaults.standard.object(forKey: kServerTokenKey)! as! String
    }
    print("Token :",strToken)
    
    let headers: HTTPHeaders = [
        "Content-Type":"application/json",
         "Accept": "application/json",
        "Authorization":  "Bearer " + strToken  // signature
    ]
   
    let url = Base_URL + strMethodname
    
    print("CheckUrl: ",url)
    print("CheckParameters: ",Details)
    
    Alamofire.request(url, method: .get, parameters: Details as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON{ response in
        print("response = ",response)
        DispatchQueue.main.async {
            completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
        }
        
        #if DEBUG
            //print("signUpResponse Debug Data: \(String(describing: response.result.value))")
        #endif
    }
}


func getallApiResultwithGetTokenMethod(strMethodname : String,Details: NSDictionary, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    var strToken = ""
    if UserDefaults.standard.object(forKey: kServerTokenKey) != nil
    {
        strToken = UserDefaults.standard.object(forKey: kServerTokenKey)! as! String
    }
    print("Token :",strToken)
    
    let headers: HTTPHeaders = [
         "Content-Type":"application/json",
         "Accept": "application/json",
        "Authorization":  "Bearer" + strToken  // signature
    ]
    
    let url = Base_URL + strMethodname
    
    print("CheckUrl: ",url)
    print("CheckParameters: ",Details)
    
    Alamofire.request(url, method: .get, parameters: Details as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON{ response in
        print("response = ",response)
        DispatchQueue.main.async {
            completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
        }
        
        #if DEBUG
        //print("signUpResponse Debug Data: \(String(describing: response.result.value))")
        #endif
    }
}



