//
//  ServiceHelper.swift
//  NewsDemo
//
//  Created by kushal on 03/07/19.
//  Copyright © 2019 ajeet. All rights reserved.
//

import Foundation
import UIKit
import Alamofire



func getAllApiResults(Details: NSDictionary,pageNo: Int64, completionHandler : @escaping (NSDictionary?, NSError?) -> ()) {
    let url = "https://newsapi.org/v2/everything?q=bitcoin&from=2019-06-06&sortBy=publishedAt&apiKey=6b8f6d7100044d0e8797e231a8e73dfa&page=\(pageNo)"
    
    print("ApiURL ==========>> ",url)
    print("ApiParameters: ==========>>",Details)
    
    
    Alamofire.request(url, method: .get, parameters: Details as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON{ response in
        print("API_Response ========>> ", response)
        DispatchQueue.main.async {
            completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
        }
        
        #if DEBUG
        //print("signUpResponse Debug Data: \(String(describing: response.result.value))")
        #endif
    }
}


