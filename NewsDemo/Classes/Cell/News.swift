//
//  News.swift
//  NewsDemo
//
//  Created by kushal on 02/07/19.
//  Copyright © 2019 ajeet. All rights reserved.
//

import UIKit

class News: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblAuther: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblContent: UILabel!
    @IBOutlet var imgNewsThumb: UIImageView!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
