//
//  WebViewVC.swift
//  NewsDemo
//
//  Created by kushal on 03/07/19.
//  Copyright © 2019 ajeet. All rights reserved.
//

import UIKit

class WebViewVC: UIViewController,UIWebViewDelegate {
    
    //MARK:- Outlets
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var webView: UIWebView!
    
    //MARK:- Variables
    
    var OpenNewsURl : URL!
    
    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
         // Do any additional setup after loading the view.
        
        activity.startAnimating()
        let setURLRequest=URLRequest(url:OpenNewsURl!)
        webView.loadRequest(setURLRequest)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
     }
 
    //MARK:- Web View Methods
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activity.stopAnimating()
        activity.isHidden = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        activity.stopAnimating()
        activity.isHidden = true
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
