//
//  NewsHomeVC.swift
//  NewsDemo
//
//  Created by kushal on 03/07/19.
//  Copyright © 2019 ajeet. All rights reserved.
//

import UIKit
import SDWebImage

class NewsHomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    //MARK:- Outlets
    @IBOutlet var tblNews: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    //MARK:- Variables
    var arrNewsData = [Article]()
    var filterNewsData = [Article]()
    var currentArticle: Article?
    var arrTitle = [String]()
    var refreshControl = UIRefreshControl()
    var currentPage: Int64 = 1
    var totalCount: Int64 = 0
    var searchActive : Bool = false
    var filtered:[String] = []
    @IBOutlet var lblNoResultFound: UILabel!
    
     //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tblNews.estimatedRowHeight = 82
        tblNews.rowHeight =  UITableView.automaticDimension
        if appDelegate.isInternetAvailable() == true {
            self.showActivity(text: "")
            self.performSelector(inBackground: #selector(self.newsApiCAlling), with: nil)
        }
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        tblNews.addSubview(refreshControl)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func refresh(sender: UIRefreshControl) {
        self.view.endEditing(true)
        currentPage = 1
        searchBar.text = ""
        if appDelegate.isInternetAvailable() == true {
            self.performSelector(inBackground: #selector(self.newsApiCAlling), with: nil)
        }
    }
    
    func showHideNoDataLabel() {
        if searchActive == true {
            if filterNewsData.count > 0 {
                lblNoResultFound.isHidden = true
            } else {
                lblNoResultFound.isHidden = false
            }
        } else {
            if arrNewsData.count > 0 {
                lblNoResultFound.isHidden = true
            } else {
                lblNoResultFound.isHidden = false
            }
        }
    }
    //MARK:- IBAction
    @IBAction func btntryAgain_DidSelect(_ sender: Any) {
         if appDelegate.isInternetAvailable() == true {
            self.showActivity(text: "")
            self.performSelector(inBackground: #selector(self.newsApiCAlling), with: nil)
        }
    }
    
    //MARK:- UISearchBar Delegates
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//            searchActive = true;
//    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
     private func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchActive = false
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchActive = false
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
         let text = searchBar.text ?? ""
        if searchText.count == 0 {
            searchActive = false
          }
        
        self.filterNewsData = arrNewsData.filter { (obj) -> Bool in
            return obj.title.range(of: text, options: .caseInsensitive) != nil
         }
        
        if(filterNewsData.count > 0) {
            searchActive = true
        }
        
        self.showHideNoDataLabel()
        self.tblNews.reloadData()
    }
    
    //MARK:- TableViewDelegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
             return filterNewsData.count
        }
        else{
            return arrNewsData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "News", for: indexPath) as! News
        cell.selectionStyle = .none
         if(searchActive) {
              currentArticle = filterNewsData[indexPath.row]
           }
        else {
              currentArticle = arrNewsData[indexPath.row]
        }
        cell.lblTitle.text  = currentArticle?.title
        cell.lblAuther.text  =  currentArticle?.author
        cell.lblContent.text  = currentArticle?.articleDescription
        let newsTime =  getCreatePostTime(strdate: currentArticle?.publishedAt ?? "")
        cell.lblTime.text  = newsTime
        cell.imgNewsThumb.sd_addActivityIndicator()
        
        let url = URL.init(string:currentArticle?.urlToImage ?? "" )
        cell.imgNewsThumb.sd_setImage(with: url, placeholderImage: UIImage.init(named: "NoImage"), options: .refreshCached, completed: { (img, error, cacheType, url) in
            cell.imgNewsThumb.sd_removeActivityIndicator()
        })
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedIndex =  indexPath.row
        var strNewsUrl = ""
        let dic = arrNewsData[selectedIndex]
        strNewsUrl = dic.url
        let setURL = URL(string:strNewsUrl)
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        nav.OpenNewsURl = setURL
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == arrNewsData.count - 2 {
            if totalCount > arrNewsData.count {
                currentPage += 1
                if appDelegate.isInternetAvailable() == true {
                    self.performSelector(inBackground: #selector(self.newsApiCAlling), with: nil)
                }
            }
        }
    }
    //MARK:- NEWS API Calling
    
    @objc func newsApiCAlling () {
        let pageNo = currentPage
        getAllApiResults(Details: NSDictionary(), pageNo: pageNo) { (responseData, error) in
            if error == nil {
                self.hideActivity()
                self.refreshControl.endRefreshing()
                let dicData = responseData?.dictionaryByReplacingNullsWithBlanks()
                if let checkPlan = dicData, (dicData?.object(forKey: "status") as? String) == "error"{
                    self.showHideNoDataLabel()
                    self.onShowAlertController(title: "Error" , message:  (checkPlan["message"] as? String))
                    return
                }
                else if let response = dicData, (dicData?.object(forKey: "status") as! String) == "ok"{
                    do {
                        let data =  try JSONSerialization.data(withJSONObject: response, options: JSONSerialization.WritingOptions.prettyPrinted)
                        // first of all convert json to the data
                        if let convertedString = String(data: data, encoding: String.Encoding.utf8){
                            let news = try NewsJSON(convertedString)
                            if pageNo == 1 {
                                self.arrNewsData.removeAll()
                            }
                            self.arrNewsData.append(contentsOf: news.articles)
                            self.showHideNoDataLabel()
                            
                            self.totalCount = news.totalResults
                            self.tblNews.reloadData()
                        }
                    } catch let myJSONError {
                        print(myJSONError)
                        self.showHideNoDataLabel()
                    }
                }
                
            }else {
                self.hideActivity()
                self.refreshControl.endRefreshing()
                self.showHideNoDataLabel()
                self.onShowAlertController(title: "Error" , message: "Having some issue.Please try again.")
              }
        }
    }
}


